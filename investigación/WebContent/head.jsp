<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
	<link rel="icon" href="assets/img/educacionadventistaicon.png"><!-- ICONO SUPERIOR -->
	<meta name="description" content="Página web informativa de Investigación en el ITSAE-CADE" />
	<meta name="author" content="Miguel Miño" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Carga los flags de meta para diseÃ±o responsivo -->
    <!-- Carga el font utilizado para los tÃ­tulos -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- Carga los archivos de CSS de Boostrap 3 -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!-- Carga los archivos de CSS personalizados nuestros -->
    <link rel="shortcut icon" type="image/x-icon" href="../favicon.ico">
    <!-- BOOTSTRAP CORE CSS -->
	<link href="assets/css/bootstrap.css" rel="stylesheet" />
	<!-- ION ICONS STYLES -->
	<link href="assets/css/ionicons.css" rel="stylesheet" />
	<!-- FONT AWESOME ICONS STYLES -->
	<link href="assets/css/font-awesome.css" rel="stylesheet" />
	<!-- FANCYBOX POPUP STYLES -->
	<link href="assets/js/source/jquery.fancybox.css" rel="stylesheet" />
	<!-- STYLES FOR VIEWPORT ANIMATION -->
	<link href="assets/css/animations.min.css" rel="stylesheet" />
</head>