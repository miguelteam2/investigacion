﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en" class="no-js" >
<%@ include file="head.jsp" %>
<head>
<%  String mensaje = ""; int msj;
	if(request.getParameter("msj")!=null){
		msj = Integer.valueOf(request.getParameter("msj"));}else{msj=0;}
	switch (msj){
	 case 5: mensaje = "Gracias por Ayudarnos a Crecer"; break;
	 case 6: mensaje = "Hubo un problema al registrar tu Idea, Inténtalo más tarde"; break;
	 default: break;
	}  
%>
<title>ITSAE || Dpto. Investigación</title>
<link href="assets/css/style-green.css" rel="stylesheet" />
</head>
<body data-spy="scroll" data-target="#menu-section">
<!--MENU SECTION START-->
<div class="navbar navbar-inverse navbar-fixed-top scroll-me" id="menu-section" >
<div class="container">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="#">Dpto. de Investigación</a>
</div>
<div class="navbar-collapse collapse">
<ul class="nav navbar-nav navbar-right">
<li><a href="#home">INICIO</a></li>
<li><a href="#services">SERVICIOS</a></li>
<!--li><a href="#pricing">PRICING</a></li-->
<li><a href="#work">TRABAJOS</a></li>
<li><a href="#team">EQUIPO</a></li>
<li><a href="#ideas">APORTA</a></li>
<li><a href="#contact">CONTACTOS</a></li>
<li><a href="login.jsp">LOG IN</a></li>
</ul>
</div>

</div>
</div>
<!--MENU SECTION END-->
<!--HOME SECTION START-->
<div id="home" >
<div class="container">
<div class="row">
<div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 ">
<div id="carousel-slider" data-ride="carousel" class="carousel slide  animate-in" data-anim-type="fade-in-up">

<div class="carousel-inner">
<div class="item active">

<h3>
Consectetur adipiscing elit felis dolor felis dolor vitae
</h3>
<p>
Lorem ipsumdolor sitamet, consect adipiscing elit
Lorem ipsumdolor sitamet, consect adipiscing elit.
Lorem ipsumdolor sitamet, consect adipiscing elit
Lorem ipsumdolor sitamet, consect adipiscing elit.
</p>
</div>
<div class="item">
<h3>
Lorem ipsumdolor sitamet, consect adipiscing elit
</h3>
<p>
Lorem ipsumdolor sitamet, consect adipiscing elit
Lorem ipsumdolor sitamet, consect adipiscing elit.
Lorem ipsumdolor sitamet, consect adipiscing elit
Lorem ipsumdolor sitamet, consect adipiscing elit.
</p>
</div>

</div>


</div>


</div>
</div>
<div class="row animate-in" data-anim-type="fade-in-up">
<div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-8 col-lg-offset-2 scroll-me">


<p >
Síguenos en nuestras cuentas de :

</p>
<div class="social">
<a href="https://www.facebook.com/groups/552572511562516/?fref=ts" class="btn button-custom btn-custom-one" target="_blank" ><i class="fa fa-facebook "></i></a>
<!--a href="#" class="btn button-custom btn-custom-one" ><i class="fa fa-twitter"></i></a-->
<a href="#" class="btn button-custom btn-custom-one" ><i class="fa fa-google-plus " target="_blank"></i></a>
<!--a href="#" class="btn button-custom btn-custom-one" ><i class="fa fa-linkedin "></i></a-->
<!--a href="#" class="btn button-custom btn-custom-one" ><i class="fa fa-pinterest "></i></a-->
<!--a href="#" class="btn button-custom btn-custom-one" ><i class="fa fa-github "></i></a-->
</div>
<a href="#services" class=" btn button-custom btn-custom-two">Ver nuestros Servicios </a>
</div>
</div>
</div>

</div>

<!--HOME SECTION END-->
<!--SERVICE SECTION START-->
<section id="services" >
<div class="container">
<div class="row text-center header">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 animate-in" data-anim-type="fade-in-up">
<h3>Nuestros Servicios</h3>
<hr />
</div>
</div>
<div class="row animate-in" data-anim-type="fade-in-up">
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="services-wrapper">
<i class="ion-document"></i>
<h3>Investigaciones</h3>
Morbi mollis lectus et ipsum sollicitudin varius.
Aliquam tempus ante placerat, consectetur tellus nec, porttitor nulla.
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="services-wrapper">
<i class="ion-scissors"></i>
<h3>Corregimos tus Documentos</h3>
Morbi mollis lectus et ipsum sollicitudin varius.
Aliquam tempus ante placerat, consectetur tellus nec, porttitor nulla.
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="services-wrapper">
<i class="ion-clipboard"></i>
<h3>Guia de Tesis</h3>
Morbi mollis lectus et ipsum sollicitudin varius.
Aliquam tempus ante placerat, consectetur tellus nec, porttitor nulla.
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="services-wrapper">
<i class="ion-calendar"></i>
<h3>Horarios de Atención</h3>
Morbi mollis lectus et ipsum sollicitudin varius.
Aliquam tempus ante placerat, consectetur tellus nec, porttitor nulla.
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="services-wrapper">
<i class="ion-erlenmeyer-flask"></i>
<h3>Investigación Química</h3>
Morbi mollis lectus et ipsum sollicitudin varius.
Aliquam tempus ante placerat, consectetur tellus nec, porttitor nulla.
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="services-wrapper">
<i class="ion-monitor"></i>
<h3>Investigación Informática</h3>
Morbi mollis lectus et ipsum sollicitudin varius.
Aliquam tempus ante placerat, consectetur tellus nec, porttitor nulla.
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="services-wrapper">
<i class="ion-pinpoint"></i>
<h3>Objetivos</h3>
Morbi mollis lectus et ipsum sollicitudin varius.
Aliquam tempus ante placerat, consectetur tellus nec, porttitor nulla.
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="services-wrapper">
<i class="ion-tshirt-outline"></i>
<h3>...........</h3>
Morbi mollis lectus et ipsum sollicitudin varius.
Aliquam tempus ante placerat, consectetur tellus nec, porttitor nulla.
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="services-wrapper">
<i class="ion-speedometer"></i>
<h3>Rendimiento Itsae</h3>
Morbi mollis lectus et ipsum sollicitudin varius.
Aliquam tempus ante placerat, consectetur tellus nec, porttitor nulla.
</div>
</div>
</div>
</div>
</section>

<!--SERVICE SECTION END-->
<!--PRICING SECTION START-->
<!--section id="pricing" >
<div class="container">
<div class="row text-center header animate-in" data-anim-type="fade-in-up">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<h3>Opciones de Precios</h3>
<hr />
</div>
</div>
<div class="row pad-bottom animate-in" data-anim-type="fade-in-up">
<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
<div class="row db-padding-btm db-attached">
<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
<div class="light-pricing">
<div class="price">
<sup>$</sup>99
<small>per day</small>
</div>
<div class="type">
BASIC
</div>
<ul>

<li><i class="glyphicon glyphicon-print"></i>30+ Accounts </li>
<li><i class="glyphicon glyphicon-time"></i>150+ Projects </li>
<li><i class="glyphicon glyphicon-trash"></i>Lead Required</li>
</ul>
<div class="pricing-footer">

<a href="#" class="btn button-custom btn-custom-one">BOOK ORDER</a>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
<div class="light-pricing popular">
<div class="price">
<sup>$</sup>199
<small>per month</small>
</div>
<div class="type">
MEDIUM
</div>
<ul>

<li><i class="glyphicon glyphicon-print"></i>30+ Accounts </li>
<li><i class="glyphicon glyphicon-time"></i>150+ Projects </li>
<li><i class="glyphicon glyphicon-trash"></i>Lead Required</li>
</ul>
<div class="pricing-footer">

<a href="#" class="btn button-custom btn-custom-one">BOOK ORDER</a>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
<div class="light-pricing">
<div class="price">
<sup>$</sup>249
<small>per yer</small>
</div>
<div class="type">
ADVANCE
</div>
<ul>

<li><i class="glyphicon glyphicon-print"></i>30+ Accounts </li>
<li><i class="glyphicon glyphicon-time"></i>150+ Projects </li>
<li><i class="glyphicon glyphicon-trash"></i>Lead Required</li>
</ul>
<div class="pricing-footer">

<a href="#" class="btn button-custom btn-custom-one">BOOK ORDER</a>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
<div class="light-pricing">
<div class="price">
<sup>$</sup>599
<small>one time</small>
</div>
<div class="type">
EXTENDED
</div>
<ul>

<li><i class="glyphicon glyphicon-print"></i>30+ Accounts </li>
<li><i class="glyphicon glyphicon-time"></i>150+ Projects </li>
<li><i class="glyphicon glyphicon-trash"></i>Lead Required</li>
</ul>
<div class="pricing-footer">

<a href="#" class="btn button-custom btn-custom-one">BOOK ORDER</a>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="row animate-in" data-anim-type="fade-in-up">
<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
<div class="row db-padding-btm db-attached">

<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="light-pricing ">
<div class="price">
<sup>$</sup>99
<small>per day</small>
</div>
<div class="type">
SMALL
</div>
<ul>

<li><i class="glyphicon glyphicon-print"></i>30+ Accounts </li>
<li><i class="glyphicon glyphicon-time"></i>150+ Projects </li>
<li><i class="glyphicon glyphicon-trash"></i>Lead Required</li>
</ul>
<div class="pricing-footer">

<a href="#" class="btn button-custom btn-custom-one">BOOK ORDER</a>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="light-pricing popular">
<div class="price">
<sup>$</sup>159
<small>per month</small>
</div>
<div class="type">
MEDIUM
</div>
<ul>

<li><i class="glyphicon glyphicon-print"></i>30+ Accounts </li>
<li><i class="glyphicon glyphicon-time"></i>150+ Projects </li>
<li><i class="glyphicon glyphicon-trash"></i>Lead Required</li>
</ul>
<div class="pricing-footer">

<a href="#" class="btn button-custom btn-custom-one">BOOK ORDER</a>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="light-pricing">
<div class="price">
<sup>$</sup>799
<small>per month</small>
</div>
<div class="type">
ADVANCE
</div>
<ul>

<li><i class="glyphicon glyphicon-print"></i>30+ Accounts </li>
<li><i class="glyphicon glyphicon-time"></i>150+ Projects </li>
<li><i class="glyphicon glyphicon-trash"></i>Lead Required</li>
</ul>
<div class="pricing-footer">

<a href="#" class="btn button-custom btn-custom-one">BOOK ORDER</a>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
</section-->
<!--PRIICING SECTION END-->
<!--WORK SECTION START-->
<section id="work" >
<div class="container">
<div class="row text-center header animate-in" data-anim-type="fade-in-up">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<h3>Nuestros Trabajos</h3>
<hr />
</div>
</div>
<div class="row text-center animate-in" data-anim-type="fade-in-up" >
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pad-bottom">
<div class="caegories">
<a href="#" data-filter="*" class="active btn btn-custom btn-custom-two btn-sm">Todos</a>
<a href="#" data-filter=".html" class="btn btn-custom btn-custom-two btn-sm">Informática</a>
<a href="#" data-filter=".css" class="btn btn-custom btn-custom-two btn-sm">Nutrición</a>
<a href="#" data-filter=".code" class="btn btn-custom btn-custom-two btn-sm" >Teología</a>
<a href="#" data-filter=".script" class="btn btn-custom btn-custom-two btn-sm" >Administración</a>
</div>
</div>
</div>
<div class="row text-center animate-in" data-anim-type="fade-in-up" id="work-div">

<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 html">
<div class="work-wrapper">

<a class="fancybox-media" title="Image Title Goes Here" href="assets/img/portfolio/1.jpg">

<img src="assets/img/portfolio/1.jpg" class="img-responsive img-rounded" alt="" />
</a>

<h4>Morbi mollis lectus et ipsum</h4>
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 html css">
<div class="work-wrapper">

<a class="fancybox-media" title="Image Title Goes Here" href="assets/img/portfolio/2.jpg">

<img src="assets/img/portfolio/2.jpg" class="img-responsive img-rounded" alt="" />
</a>

<h4>Morbi mollis lectus et ipsum</h4>
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 code script">
<div class="work-wrapper">

<a class="fancybox-media" title="Image Title Goes Here" href="assets/img/portfolio/3.jpg">

<img src="assets/img/portfolio/3.jpg" class="img-responsive img-rounded" alt="" />
</a>

<h4>Morbi mollis lectus et ipsum</h4>
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 html script">
<div class="work-wrapper">

<a class="fancybox-media" title="Image Title Goes Here" href="assets/img/portfolio/4.jpg">

<img src="assets/img/portfolio/4.jpg" class="img-responsive img-rounded" alt="" />
</a>

<h4>Morbi mollis lectus et ipsum</h4>
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 html code">
<div class="work-wrapper">

<a class="fancybox-media" title="Image Title Goes Here" href="assets/img/portfolio/5.jpg">

<img src="assets/img/portfolio/5.jpg" class="img-responsive img-rounded" alt="" />
</a>

<h4>Morbi mollis lectus et ipsum</h4>
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 script">
<div class="work-wrapper">

<a class="fancybox-media" title="Image Title Goes Here" href="assets/img/portfolio/6.jpg">

<img src="assets/img/portfolio/6.jpg" class="img-responsive img-rounded" alt="" />
</a>

<h4>Morbi mollis lectus et ipsum</h4>
</div>
</div>
</div>
</div>
</section>
<!--WORK SECTION END-->
<!--TEAM SECTION START-->
<section id="team" >
<div class="container">
<div class="row text-center header animate-in" data-anim-type="fade-in-up">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<h3>Miembros del Equipo </h3>
<hr />
</div>
</div>
<div class="row animate-in" data-anim-type="fade-in-up">

<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
<div class="team-wrapper">
<div class="team-inner" style="background-image: url('assets/img/team/1.jpg')" >
<a href="#" target="_blank" > <i class="fa fa-twitter" ></i></a>
</div>
<div class="description">
<h3>Ing. Everth Calep Flores</h3>
<h5> <strong> Developer & Designer </strong></h5>
<p>
Pellentesque elementum dapibus convallis.
Vivamus eget finibus massa.
</p>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
<div class="team-wrapper">
<div class="team-inner" style="background-image: url('assets/img/team/2.jpg')" >
<a href="#" target="_blank" > <i class="fa fa-twitter" ></i></a>
</div>
<div class="description">
<h3> Mg. Lennin Guamán</h3>
<h5> <strong> Developer & Designer </strong></h5>
<p>
Pellentesque elementum dapibus convallis.
Vivamus eget finibus massa.
</p>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
<div class="team-wrapper">
<div class="team-inner" style="background-image: url('assets/img/team/3.jpg')" >
<a href="#" target="_blank" > <i class="fa fa-twitter" ></i></a>
</div>
<div class="description">
<h3> Lic. Alfonzo Daza</h3>
<h5> <strong> Developer & Designer </strong></h5>
<p>
Pellentesque elementum dapibus convallis.
Vivamus eget finibus massa.
</p>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
<div class="team-wrapper">
<div class="team-inner" style="background-image: url('assets/img/team/4.jpg')" >
<a href="#" target="_blank" > <i class="fa fa-twitter" ></i></a>
</div>
<div class="description">
<h3> Msc.Blanca Cuchicóndor</h3>
<h5> <strong> Developer & Designer </strong></h5>
<p>
Pellentesque elementum dapibus convallis.
Vivamus eget finibus massa.
</p>
</div>
</div>
</div>
</div>
</div>
</section>
<!--TEAM SECTION END-->
<!-- ===========IDEAS======================== -->
<section id="ideas" >
<div class="container">
<div class="row text-center header">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 animate-in" data-anim-type="fade-in-up">
<h3>Aporta con tu Idea</h3>
<div align="center">
<label class="control-label" style="color:red;" ><h2><%=mensaje %></h2></label>
</div>
<hr />
</div>
</div>
<div class="row animate-in" data-anim-type="fade-in-up">
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="services-wrapper">
<%@ include file="registro_ideas.jsp" %>


</div>
</div>
</div>
</div>
</section>






<!-- ============================== -->




<!--CONTACT SECTION START-->
<section id="contact" >
<div class="container">
<div class="row text-center header animate-in" data-anim-type="fade-in-up">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

<h3>Contact Details </h3>
<hr />

</div>
</div>

<div class="row animate-in" data-anim-type="fade-in-up">

<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="contact-wrapper">
<h3>We Are Social</h3>
<p>
Aliquam tempus ante placerat,
consectetur tellus nec, porttitor nulla.
</p>
<div class="social-below">
<a href="https://www.facebook.com/groups/552572511562516/?fref=ts" target="_blank"class="btn button-custom btn-custom-two" > Facebook</a>
<!--a href="#" class="btn button-custom btn-custom-two" > Twitter</a-->
<a href="#" class="btn button-custom btn-custom-two" > Google +</a>
</div>
</div>

</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="contact-wrapper">
<h3>Quick Contact</h3>
<h4><strong>Email : </strong> investigacion@itsae.edu.ec </h4>
<h4><strong>Call : </strong> +09-88-99-77-55 </h4>
<h4><strong>Skype : </strong> Alfonzo Daza </h4>
</div>

</div>
<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
<div class="contact-wrapper">
<h3>Address : </h3>
<h4>Km 14 1/2 Via Quevedo </h4>
<h4>Santo Domingo</h4>
<h4>Ecuador</h4>
<div class="footer-div" >
&copy; 2015 ITSAE | <a href="http://www.designbootstrap.com/" target="_blank" >by Quinto Sistemas</a>
</div>
</div>

</div>

</div>


</div>
</section>
<!--CONTACT SECTION END-->

<!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME -->
<!-- CORE JQUERY -->
<script src="assets/js/jquery-1.11.1.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="assets/js/bootstrap.js"></script>
<!-- EASING SCROLL SCRIPTS PLUGIN -->
<script src="assets/js/vegas/jquery.vegas.min.js"></script>
<!-- VEGAS SLIDESHOW SCRIPTS -->
<script src="assets/js/jquery.easing.min.js"></script>
<!-- FANCYBOX PLUGIN -->
<script src="assets/js/source/jquery.fancybox.js"></script>
<!-- ISOTOPE SCRIPTS -->
<script src="assets/js/jquery.isotope.js"></script>
<!-- VIEWPORT ANIMATION SCRIPTS   -->
<script src="assets/js/appear.min.js"></script>
<script src="assets/js/animations.min.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="assets/js/custom.js"></script>
</body>

</html>
