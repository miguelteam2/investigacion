<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html>
<html lang="en">
<%@ include file="head.jsp" %>
<% String msj = request.getParameter("error"); 
	if(msj==null){
		msj="";
	}else{
		if(msj.equals("v")){
			msj="Datos incorrecto. Por favor intente de nuevo. <br> Contacte con el administrador.";
		}else{
			msj="Acceso solo a personas autorizadas";
		}
	}
%>
<head>
	<title>Login | Sistema Investigación</title>
	<link rel="stylesheet" href="assets/css/login.css">
</head>

<body onload="this.login.f_user.focus()">
    <div class="container">
        <div class="col-md-8 col-md-offset-2">
            <span class="text-center">
        <h1 class="login-head">ITSAE-CADE</h1>
        <label class="control-label" style="color:red;"><b><%=msj %></b></label>        
        <h2>Sistema Investigación</h2>
            </span>
            <form action="cLogin" method="post" name="login" >
								<div class="top-margin">
									<label>Usuario<span class="text-danger">(*)</span>:</label>
									<div class="input-group">
										<div class="input-group-addon"><span class="fa fa-user"></span></div>
										<input name="user" id="user" type="text" placeholder="Ingrese su Usuario" class="form-control">
									</div>
								</div>
								<div class="top-margin">
									<label>Contraseña<span class="text-danger">(*)</span>:</label>
									<div class="input-group">
										<div class="input-group-addon"><span class="fa fa-lock"></span></div>
										<input name="pass" id="pass" type="password" placeholder="Contraseña" class="form-control">
									</div>
								</div>

								<hr>

								<div class="row">
									<div class="col-lg-8">
										<b><a href="passforgot.jsp">Olvidaste tu contraseña?</a></b>
									</div>
									<div class="col-lg-4 text-right">
										<button class="btn btn-action" type="submit">Ingresar</button>
									</div>
								</div>
							</form>
            <div class="row">
                <div class="col-md-2 col-md-offset-5 col-xs-4 col-xs-offset-4"><img src="assets/img/educacionadventistaicon.png" alt="Logo EducaciÃ³n Adventista" class="img-responsive text-center"></div>
            </div>
        </div>
    </div>
    <!-- Carga el jQuery -->
    <script src="assets/js/jquery.js"></script>
    <!-- Carga el Javascript de Bootstrap 3 -->
    <script src="assets/js/bootstrap.min.js"></script>
</body>

</html>
