-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.0.19-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para investigacion
DROP DATABASE IF EXISTS `investigacion`;
CREATE DATABASE IF NOT EXISTS `investigacion` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `investigacion`;


-- Volcando estructura para tabla investigacion.sesiones
DROP TABLE IF EXISTS `sesiones`;
CREATE TABLE IF NOT EXISTS `sesiones` (
  `id_sesion` varchar(45) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `inicio_sesion` datetime NOT NULL,
  `ultimo_acceso` datetime NOT NULL,
  `ip` varchar(16) NOT NULL,
  `host` varchar(45) NOT NULL,
  `servername` varchar(45) NOT NULL,
  `estado` varchar(1) NOT NULL,
  UNIQUE KEY `id_sesion` (`id_sesion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla investigacion.sesiones: ~15 rows (aproximadamente)
/*!40000 ALTER TABLE `sesiones` DISABLE KEYS */;
REPLACE INTO `sesiones` (`id_sesion`, `usuario`, `nombre`, `inicio_sesion`, `ultimo_acceso`, `ip`, `host`, `servername`, `estado`) VALUES
	('112C1A19EC4D36F27F173A8CA4B652C9', 'miguel', 'null', '2015-11-26 11:09:33', '2015-11-26 11:09:33', '0:0:0:0:0:0:0:1', '0:0:0:0:0:0:0:1', 'localhost', '1'),
	('186ED3D43110F3C3613D3BC81C86CE54', 'miguel', 'null', '2015-11-26 09:37:38', '2015-11-26 09:37:38', '0:0:0:0:0:0:0:1', '0:0:0:0:0:0:0:1', 'localhost', '1'),
	('1FE464B1D71A601CB6F2042BA8945C22', 'miguel', 'null', '2015-11-26 10:08:40', '2015-11-26 10:08:40', '0:0:0:0:0:0:0:1', '0:0:0:0:0:0:0:1', 'localhost', '1'),
	('364CC56295A7AFCDD7B3758587027C6A', 'miguel', 'null', '2015-11-26 11:08:12', '2015-11-26 11:08:12', '0:0:0:0:0:0:0:1', '0:0:0:0:0:0:0:1', 'localhost', '1'),
	('50743A4F065E2F2A447F53A6C14BBE9C', 'miguel', 'null', '2015-11-26 10:25:15', '2015-11-26 10:25:15', '0:0:0:0:0:0:0:1', '0:0:0:0:0:0:0:1', 'localhost', '1'),
	('76C5A3DAAE6E69183F7D3142F7D50361', 'miguel', 'null', '2015-11-26 11:07:46', '2015-11-26 11:07:46', '0:0:0:0:0:0:0:1', '0:0:0:0:0:0:0:1', 'localhost', '1'),
	('90435A6F5CF55A1862D947E29BF405A6', 'miguel', 'null', '2015-11-26 11:07:02', '2015-11-26 11:07:02', '0:0:0:0:0:0:0:1', '0:0:0:0:0:0:0:1', 'localhost', '1'),
	('93D673FEB04AB9F3B36E6CF44B8B2A0D', 'miguel', 'null', '2015-11-26 09:38:47', '2015-11-26 09:38:47', '0:0:0:0:0:0:0:1', '0:0:0:0:0:0:0:1', 'localhost', '1'),
	('A3C664F4AAD8A5BC30375F1092988F2A', 'miguel', 'null', '2015-11-26 08:54:44', '2015-11-26 08:54:44', '0:0:0:0:0:0:0:1', '0:0:0:0:0:0:0:1', 'localhost', '1'),
	('B8CE37693A3BD5992B43ADF0A01E39DB', 'miguel', 'null', '2015-11-26 10:01:04', '2015-11-26 10:01:04', '0:0:0:0:0:0:0:1', '0:0:0:0:0:0:0:1', 'localhost', '1'),
	('B968247ADD2E804746F8A7E9109A42AC', 'miguel', 'null', '2015-11-26 10:00:32', '2015-11-26 10:00:32', '0:0:0:0:0:0:0:1', '0:0:0:0:0:0:0:1', 'localhost', '1'),
	('C9CAADE15637D92FB0E4439C6ED30C6E', 'miguel', 'null', '2015-11-26 11:07:12', '2015-11-26 11:07:12', '0:0:0:0:0:0:0:1', '0:0:0:0:0:0:0:1', 'localhost', '1'),
	('E373DDF4E08C5000386308BA9E85CA09', 'miguel', 'null', '2015-11-26 10:01:55', '2015-11-26 10:01:55', '0:0:0:0:0:0:0:1', '0:0:0:0:0:0:0:1', 'localhost', '1'),
	('E99D6A7157610E82637A71C81482275B', 'miguel', 'null', '2015-11-27 09:47:48', '2015-11-27 09:47:48', '0:0:0:0:0:0:0:1', '0:0:0:0:0:0:0:1', 'localhost', '1'),
	('EC6FD792E8DC553364A5F87605F8BF50', 'miguel', 'null', '2015-11-26 11:06:42', '2015-11-26 11:06:42', '0:0:0:0:0:0:0:1', '0:0:0:0:0:0:0:1', 'localhost', '1');
/*!40000 ALTER TABLE `sesiones` ENABLE KEYS */;


-- Volcando estructura para tabla investigacion.usuario
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(50) NOT NULL,
  `password` varchar(45) NOT NULL,
  `estado` varchar(1) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `categoria` varchar(45) NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla investigacion.usuario: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
REPLACE INTO `usuario` (`iduser`, `usuario`, `password`, `estado`, `nombre`, `apellido`, `categoria`) VALUES
	(36, 'miguel', '123456', '1', 'Miguel', 'Miño', 'estudiante');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
