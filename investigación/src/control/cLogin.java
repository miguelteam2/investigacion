package control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.Login;
import modelo.Sesion;

@WebServlet("/cLogin")
public class cLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
	  Login l = new Login();   
	  Sesion s = new Sesion();
	    public cLogin() {
	        super();
	    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	l.setUsuario(request.getParameter("user"));	
	l.setPassword(request.getParameter("pass"));
	int v = l.validarUsuario(l);
		if(v==1){
			HttpSession sesionOK = request.getSession();
			s.setId_sesion(sesionOK.getId());
			s.setUsuario(request.getParameter("user"));
			if(s.buscarSesion(s)==1){ //busca si existe una sesion con el id y el usuario
				s.actualizarSesion(s); //si existe actualiza la fecha
			}else{
				registrarSesion(sesionOK.getId(), request, response); //registra sesion
			}
			Login user = new Login();
			user = l.getUsuario(l);
			//VARIABLES EN SESION
			sesionOK.setAttribute("iduser", user.getIduser());
			sesionOK.setAttribute("usuario", user.getUsuario());
			sesionOK.setAttribute("estado", user.getEstado());
			sesionOK.setAttribute("nombre", user.getNombre());
			sesionOK.setAttribute("usuario2", user.getNombre()+" "+user.getApellidos());
			sesionOK.setAttribute("ip", request.getRemoteAddr());
			response.sendRedirect("admin.jsp");		 				
		}else{
			response.sendRedirect("login.jsp?error=v");
		}
	}
//id_sesion,usuario,inicio_sesion,ultimo_acceso,ip,host,servername,estado
	protected void registrarSesion(String idsesion,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		s.setId_sesion(idsesion);
		s.setUsuario(request.getParameter("user"));
		s.setIp(request.getRemoteAddr());
		s.setHost(request.getRemoteHost());
		s.setServername(request.getServerName());
		s.registrarSesion(s);			
	}
	
}

