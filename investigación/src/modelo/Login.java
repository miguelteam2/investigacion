package modelo;

import java.sql.ResultSet;

import modelo.conexion;

public class Login {
	private String iduser;
	private String usuario;
	private String password;
	private String estado;
	private String nombre;
	private String apellidos;
	
	// iduser, codigo, usuario, password, estado, nombre, apellido 
	conexion cx = new conexion();
	//metodo constructor
	public Login(){
		
	}
	//metodo Get y Set
	public String getIduser() {
		return iduser;
	}
	public void setIduser(String iduser) {
		this.iduser = iduser;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String codigo) {
		this.usuario = codigo;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	// iduser, codigo, usuario, password, estado, nombre, apellido
	//Metodos y operaciones
	public int validarUsuario(Login l){
		cx.con();
		String com = "SELECT * FROM usuario " +
				"WHERE usuario='"+l.getUsuario()+"' " +
						"AND password='"+l.getPassword()+"' " +
								"AND estado='1'";
		int res = cx.contarFilas(com);
		cx.desconectar();
		System.out.println(com);
		return res;
	}
	public Login getUsuario(Login l){		
		Login user = new Login();
		String com = "SELECT * FROM usuario " +
				"WHERE usuario='"+l.getUsuario()+"' " +
						"AND password='"+l.getPassword()+"' " +
								"AND estado='1'";
		try {
			cx.con();
			ResultSet rs = cx.getDatos(com);
			while(rs.next()){
				user.setIduser(rs.getString(1));
				user.setUsuario(rs.getString(2));
				user.setEstado(rs.getString(4));
				user.setNombre(rs.getString(5));
				user.setApellidos(rs.getString(6));	
			}
			System.out.println("1-"+com+user.getUsuario());
			return user;			
		} catch (Exception e) {
			System.out.println("2-"+com);
			return user=null;
		}
	}
}
